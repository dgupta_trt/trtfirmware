/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

SDADC_HandleTypeDef hsdadc3;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t current_address[4] = {0x00,0x00,0x00};//,0x00};
uint8_t start_address[4] = {0x00,0x00,0x00};//,0x00};
uint8_t read_address[4] = {0x00,0x00,0x00};//,0x00};
uint8_t Start_Packet[20] = {0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
		0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE};
extern uint16_t* GPIO_write_high_E0;// = (uint16_t *)0x48001018;
extern uint16_t* GPIO_write_low_E0;// = (uint16_t *)0x4800101A;
const int* time_pointer = (int *)0x40002800;
const int* time_subsecond = (int *)0x4002828;
const int* time_date = (int *)0x4002804;
int* rtc_init_register = (int *)0x400280C;
int* rtc_writeprotect = (int *)0x4002824;
int* rtc_controlregister = (int *)0x4002808;
uint32_t* spi2_pointer = (uint32_t *)0x40003800;
uint32_t* spi2_control = (uint32_t *)0x40003802;
uint32_t* spi2_status = (uint32_t *)0x40003808;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_RTC_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SDADC3_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Erase_Memory_Chip();
uint8_t Read_Status_Register();
void Read_JEDEC_ID();
void Send_OneVal_SDADC_Data();
void Store_SDADC_Data(uint8_t address[4], uint8_t adcdata[20]);
uint8_t* Read_SDADC_Data(uint8_t address[4]);
uint8_t* Read_SDADC();
uint8_t* DateStamp();
uint8_t* TimeStamp();
uint8_t* Get_BT_Data_Packet(uint8_t timestamp[4], uint8_t sdadc_vals[14]);
void Increment_Current_Address();
void Increment_Read_Address();
_Bool Compare_Mem_Address(uint8_t addr1[3/*4*/], uint8_t addr2[3/*4*/]);
_Bool Read_Check_Start_Packet(uint8_t address[3/*4*/]);
void Store_256Bytes_Data(uint8_t address[3/*4*/], uint8_t adcdata[240]);
uint8_t* Read_240Bytes_Data(uint8_t address[3/*4*/]);
uint8_t* Get_12_BT_Packets();
void Transmit_20_Bytes(uint8_t* DataBytes);
void Set_4Byte_Address();
void Mem_Write_Enable();
uint8_t Read_Mem_SecReg();
uint8_t Read_Mem_ConfigReg();

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  MX_SDADC3_Init();

  /* USER CODE BEGIN 2 */
  //Set_4Byte_Address(); //4 Byte Addressing
  Mem_Write_Enable();
  uint8_t StatusRegister = Read_Status_Register();
  if ( ( (StatusRegister >> 1) & 1) && ~(StatusRegister & 1) ) { // Check Write Enable
	  Erase_Memory_Chip();
  }
  HAL_Delay(7000);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	  	  // Check BT Connection Status, Working Implementation of BT and Mem Comm
	  	  _Bool BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
	  	  _Bool MEM_Check = Compare_Mem_Address(start_address, current_address);

	  	  // case of connected and at the starting memory location (don't need to divert to memory)
	  	  while(BT_Check && MEM_Check) {
	  		  // Internal SDADC + Timestamp
	  		  __HAL_UART_FLUSH_DRREGISTER(&huart2);
	  		  HAL_UART_Transmit(&huart2, Get_BT_Data_Packet(TimeStamp(),Read_SDADC() ), 20, 100);
	  		  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
	  		  MEM_Check = Compare_Mem_Address(start_address, current_address);
	  		  // End Internal SDADC Value + Timestamp
	  	  }
	  	  // case of connected but not at starting memory location, read from memory and transmit, reset memory location
	  	  while( BT_Check && !MEM_Check){
	  		  // SPI Communication with Memory
	  		  	  uint8_t StatusRegisterRead = Read_Status_Register();
	  		  	  if( ~( (StatusRegisterRead >> 0) & 1) )  { // We are allowed to read
	  		  		 // _Bool Start_Packet = Read_Check_Start_Packet(read_address);
	  		  		 // if(Start_Packet){
	  		  		//	  HAL_UART_Transmit(&huart2, Read_SDADC_Data(read_address), 20, 100); //start packet
	  		  		//	  Increment_Read_Address();
	  				//	  HAL_UART_Transmit(&huart2, Read_SDADC_Data(read_address), 20, 100);// data packet
	  		  		 // 	  __HAL_UART_FLUSH_DRREGISTER(&huart2);
	  		  		      HAL_UART_Transmit(&huart2, Read_240Bytes_Data(read_address), 240, 100);
	  		  		 //     Transmit_20_Bytes(Read_240Bytes_Data(read_address));
	  		  		      Increment_Read_Address();
	  					  _Bool Read_Current = Compare_Mem_Address(read_address, current_address);
	  					  if(Read_Current){
	  						  for(size_t j = 0; j < 3; j++){ // j = 4
	  							  current_address[j] = start_address[j];
	  						  }
	  						  Mem_Write_Enable();
	  						  uint8_t StatusRegisterErase = Read_Status_Register();
	  						  if ( ( (StatusRegisterErase >> 1) & 1) && ~(StatusRegisterErase & 1) ) { // Write Enable
	  							  Erase_Memory_Chip();
	  						  }
	  					  }
	  		  	//	  }
	  		  	  }

	  		  	  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
	  		  	  MEM_Check = Compare_Mem_Address(start_address, current_address);
	  	  }

	  	  // case of not connected, store to memory starting at current address
	  	  while( !BT_Check){
	  		  	  Mem_Write_Enable();

	  		  	  uint8_t StatusRegisterWrite = Read_Status_Register();
	  		  	  if ( ( (StatusRegisterWrite >> 1) & 1) && ~(StatusRegisterWrite & 1) ) {
	  		  		  //if(Mem_Check){
	  		  			//  Store_Start_Packet();
	  		  		  //}
	  		  	      //Store_SDADC_Data(current_address,Get_BT_Data_Packet(TimeStamp(),Read_SDADC() ) );
	  		  		  Store_256Bytes_Data(current_address, Get_12_BT_Packets());
	  		  		  	  uint8_t SecurityReg = Read_Mem_SecReg();
	  		  		  	  if( ~( (SecurityReg >> 5) & 1) ){
	  		  		  			  Increment_Current_Address();
	  		  		  	  }
	  		  	  }

	  		  	  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
	  		  	  MEM_Check = Compare_Mem_Address(start_address, current_address);

	  	  }


	  Mem_Write_Enable();
	  Store_256Bytes_Data(current_address, Get_12_BT_Packets());
	  HAL_Delay(1000);
	  //uint8_t StatusRegisterWrite = Read_Status_Register();
	  //if ( ( (StatusRegisterWrite >> 1) & 1) && ~(StatusRegisterWrite & 1) ){
		  HAL_UART_Transmit(&huart2, Read_240Bytes_Data(current_address), 240, 100);
	  //}
	  Increment_Current_Address();

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_SDADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.SdadcClockSelection = RCC_SDADCSYSCLK_DIV12;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG3);

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* RTC init function */
static void MX_RTC_Init(void)
{

  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initialize RTC and set the Time and Date 
    */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

  sDate.WeekDay = RTC_WEEKDAY_SUNDAY;
  sDate.Month = RTC_MONTH_APRIL;
  sDate.Date = 0x2;
  sDate.Year = 0x17;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SDADC3 init function */
static void MX_SDADC3_Init(void)
{

  SDADC_ConfParamTypeDef ConfParamStruct;

    /**Configure the SDADC low power mode, fast conversion mode,
    slow clock mode and SDADC1 reference voltage 
    */
  hsdadc3.Instance = SDADC3;
  hsdadc3.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
  hsdadc3.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
  hsdadc3.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
  hsdadc3.Init.ReferenceVoltage = SDADC_VREF_EXT;
  if (HAL_SDADC_Init(&hsdadc3) != HAL_OK)
  {
    Error_Handler();
  }

    /**Set parameters for SDADC configuration 0 Register 
    */
  ConfParamStruct.InputMode = SDADC_INPUT_MODE_SE_ZERO_REFERENCE;
  ConfParamStruct.Gain = SDADC_GAIN_1;
  ConfParamStruct.CommonMode = SDADC_COMMON_MODE_VSSA;
  ConfParamStruct.Offset = 0;
  if (HAL_SDADC_PrepareChannelConfig(&hsdadc3, SDADC_CONF_INDEX_0, &ConfParamStruct) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 112500;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PE0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
uint8_t Read_Status_Register(){
	uint8_t StatusRegister[2] = {0x00,0x00};
	uint8_t RDSR[2] = {0x05,0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	HAL_SPI_TransmitReceive(&hspi2,RDSR,StatusRegister,2,HAL_GetTick()); // Read Status
	//HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_SET); // CS High
	return StatusRegister[1];
}

void Read_JEDEC_ID(){
	  uint8_t aTxBuffer[4] = {0x9F,0x00,0x00,0x00};
	  uint8_t databuffer[4];
	  HAL_SPIEx_FlushRxFifo(&hspi2);
	  HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET);
	  switch(HAL_SPI_TransmitReceive(&hspi2, aTxBuffer, databuffer, 4, 5000))
	    {
	    case HAL_OK:
	     break;

	    case HAL_TIMEOUT:
	      // A Timeout Occur ______________________________________________________
	      // Call Timeout Handler
	      break;

	      // An Error Occur ______________________________________________________
	    case HAL_ERROR:
	      // Call Timeout Handler

	      break;
	    default:
	      break;
	    }
}
// TEST ONE STORE VALUE
void Send_OneVal_SDADC_Data(){
	 HAL_SDADC_CalibrationStart(&hsdadc3,HAL_SDADC_STATE_CALIB);
	 HAL_SDADC_PollForCalibEvent(&hsdadc3,HAL_GetTick());
	 HAL_SDADC_Start(&hsdadc3);
	 HAL_SDADC_PollForConversion(&hsdadc3, HAL_GetTick());
	 uint16_t value = 0;
     value = HAL_SDADC_GetValue(&hsdadc3);
	 HAL_SDADC_Stop(&hsdadc3);
	 uint8_t Command_Address_Data[7] = {0x02,0x00,0x00,0x00,0x03,0xFF,0xFF};
	 Command_Address_Data[5] = value >> 8;
	 Command_Address_Data[6] = value;
	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_Transmit(&hspi2,Command_Address_Data,7,HAL_GetTick()); // Read Status
}


void Store_SDADC_Data(uint8_t address[4], uint8_t adcdata[20]){
	 uint8_t Command_Address_Data[25] = {0x02,0x00,0x00,0x00,0x03,0xFF,0xFF};
	 // enter address to write to
	 for(size_t i = 1;i < 5; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 // enter the data from the ADC (12 data points)
	 for(size_t j = 5; j < 25; j++ ){
		 Command_Address_Data[j] = adcdata[j-5];
	 }

	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_Transmit(&hspi2,Command_Address_Data,20,HAL_GetTick()); // Read Status
}

uint8_t* Read_SDADC_Data(uint8_t address[3/*4*/]){
	 uint8_t Command_Address_Data[28] = {0x03,0x00,0x00,0x00,0x03,0xFF,0xFF};
	 // enter address to read from
	 for(size_t i = 1;i < 4/*5*/; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 uint8_t DataOut[24/*5*/];
	 static uint8_t ReturnData[20];
	 HAL_SPIEx_FlushRxFifo(&hspi2);
	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_TransmitReceive(&hspi2,Command_Address_Data,DataOut,24/*5*/,HAL_GetTick()); // Read Status
	 for(size_t j = 4/*5*/; j < 24/*5*/; j ++){
		 ReturnData[j-4/*5*/] = DataOut[j];
	 }
	 return ReturnData;
}

uint8_t* Read_SDADC(){
	 const size_t n = 14;
	 HAL_SDADC_CalibrationStart(&hsdadc3,HAL_SDADC_STATE_CALIB);
	 HAL_SDADC_PollForCalibEvent(&hsdadc3,HAL_GetTick());
	 static uint8_t sdadc_out[14];
	 for(int i = 0; i < n-1; i= i + 2){
		 HAL_SDADC_Start(&hsdadc3);
		 HAL_SDADC_PollForConversion(&hsdadc3, HAL_GetTick());
		 uint16_t value = 0;
		 value = HAL_SDADC_GetValue(&hsdadc3);
		 HAL_SDADC_Stop(&hsdadc3);
		 sdadc_out[i+1] = value;
		 sdadc_out[i] = value >> 8;
	 }
	 return sdadc_out;
}


uint8_t* DateStamp(){

	  // allow write to RTC registers
//  	  *rtc_writeprotect = 0xCA;
//	  	  *rtc_writeprotect = 0x53;
	  	  // Check the RSF bit
//	  	  if ( ( (*rtc_init_register >> 5) & 1) && ~( (*rtc_controlregister >> 5) & 1) ){
			   static uint8_t datebuffer[3];
	  		   datebuffer[0] = 0x00;// *time_date >> 16;
	  		   datebuffer[1] = 0x00;// *time_date >> 8;
	  		   datebuffer[2] = 0x00;// *time_date;
	  		   return datebuffer;
//	  	  }
	         // Clear the RSF bit
//	         *rtc_init_register &= ~(1 << 5);
}

uint8_t* TimeStamp(){

	  // allow write to RTC registers
//  	  *rtc_writeprotect = 0xCA;
//	  	  *rtc_writeprotect = 0x53;
	  	  // Check the RSF bit
//	  	  if ( ( (*rtc_init_register >> 5) & 1) && ~( (*rtc_controlregister >> 5) & 1) ){
			   static uint8_t timebuffer[4];
	  		   timebuffer[2] = 0x00;// *time_subsecond >> 14; // subsecond + seconds
	  		   timebuffer[3] = 0x00;// *time_subsecond >> 6;  // subsecond
	  		   timebuffer[0] = *time_pointer >> 14; // hours
	  		   timebuffer[1] = *time_pointer >> 6; // minutes
	  		   timebuffer[2] |= *time_pointer << 2; // seconds
	  		   return timebuffer;
//	  	  }
	         // Clear the RSF bit
//	         *rtc_init_register &= ~(1 << 5);
}

uint8_t* Get_BT_Data_Packet(uint8_t timestamp[4], uint8_t sdadc_vals[14]){
	static uint8_t datapacket[20];
	datapacket[0] = 0x01; // channel number
	for( size_t i = 1; i < 20; i++){
		if(i < 5){
			datapacket[i] = timestamp[i-1];
		}
		else if (i == 5){
			datapacket[i] = 0x00; //heartrate value to go here
		}
		else{
			datapacket[i] = sdadc_vals[i-6];
		}
	}
	return datapacket;
}

uint8_t* Get_12_BT_Packets(){
	static uint8_t datapacket12[240];
	uint8_t* onePacket;
	for(size_t i = 0; i < 12; i++){
		onePacket = Get_BT_Data_Packet(TimeStamp(),Read_SDADC());
		for(size_t j = 0; j < 20;j++){
			datapacket12[i*20+j] = *onePacket;
			onePacket++;
		}
	}
	return datapacket12;
}

void Store_256Bytes_Data(uint8_t address[3/*4*/], uint8_t adcdata[240]){
	 uint8_t Command_Address_Data[260/*1*/] = {0x02/*12*/,0x00,0x00,0x00,0x00,0xFF,0xFF};
	 // enter address to write to
	 for(size_t i = 1;i < 4/*5*/; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 // enter the data from the ADC (240 Bytes data points)
	 for(size_t j = 4/*5*/; j < 244/*5*/; j++ ){
		 Command_Address_Data[j] = adcdata[j-4/*5*/];
	 }
	 // append zeroes to have complete 256 Byte transactions
	 for(size_t k = 244/*5*/; k < 260/*1*/; k++){
		 Command_Address_Data[k] = 0xEE;
	 }

	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_Transmit(&hspi2,Command_Address_Data,260/*1*/,HAL_GetTick()); // Write Data
}

uint8_t* Read_240Bytes_Data(uint8_t address[3/*4*/]){
	 uint8_t Command_Address_Data[260/*1*/] = {0x03/*13*/,0x00,0x00,0x00,0x00,0xFF,0xFF};
	 // enter address to read from
	 for(size_t i = 1;i < 4/*5*/; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 uint8_t DataOut[260/*1*/];
	 static uint8_t ReturnData[240];
	 HAL_SPIEx_FlushRxFifo(&hspi2);
	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_TransmitReceive(&hspi2,Command_Address_Data,DataOut,260/*1*/,HAL_GetTick()); // Read Data
	 for(size_t j = 4/*5*/; j < 244/*5*/; j++) {
		 ReturnData[j-4/*5*/] = DataOut[j];
	 }
	 return ReturnData;
}

void Transmit_20_Bytes(uint8_t* DataBytes){
	for(size_t i = 0; i < 20; i++){
		__HAL_UART_FLUSH_DRREGISTER(&huart2);
		HAL_UART_Transmit(&huart2, DataBytes, 20, 100);
		DataBytes+=20;
	}
}

void Erase_Memory_Chip(){
	 uint8_t Erase_Chip[1] = {0x60};
	 HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	 HAL_SPI_Transmit(&hspi2,Erase_Chip,1,HAL_GetTick()); // Erase Chip
	 uint8_t SecReg = Read_Mem_SecReg();
	 if( ~( (SecReg >> 6) & 1) ){}
	 else{
		 Erase_Memory_Chip();
	 }
}

void Increment_Current_Address(){
	//uint32_t current_address_int = (uint32_t) current_address[0] << 24 | (uint32_t) current_address[1] << 16 | (uint32_t) current_address[2] << 8 | (uint32_t) current_address[3];
	uint32_t current_address_int = (uint32_t) current_address[0] << 16 | (uint32_t) current_address[1] << 8 | (uint32_t) current_address[2];
	current_address_int = current_address_int + 0x100;
	current_address[0] = (current_address_int >> 16) & 0xFF;//24;
	current_address[1] = (current_address_int >> 8) & 0xFF;//16;
	current_address[2] = (current_address_int) & 0xFF;// >> 8;
	//current_address[3] = current_address_int;
}

void Increment_Read_Address(){
	//uint32_t read_address_int = (uint32_t) read_address[0] << 24 | (uint32_t) read_address[1] << 16 | (uint32_t) read_address[2] << 8 | (uint32_t) read_address[3];
	uint32_t read_address_int = (uint32_t) read_address[0] << 16 | (uint32_t) read_address[1] << 8 | (uint32_t) read_address[2];
	read_address_int = read_address_int + 0x100;
	read_address[0] = (read_address_int >> 16) & 0xFF;// 24;
	read_address[1] = (read_address_int >> 8) & 0xFF;//16;
	read_address[2] = (read_address_int& 0xFF);// >> 8;
	//read_address[3] = read_address_int;
}

_Bool Compare_Mem_Address(uint8_t addr1[3/*4*/], uint8_t addr2[3/*4*/]){
	_Bool check = 1;
	size_t i = 0;
	while(check && i < 3/*4*/){
		check = addr1[i] == addr2[i];
		i++;
	}
	return check;
}

_Bool Read_Check_Start_Packet(uint8_t address[4]){
	 uint8_t Command_Address_Data[28] = {0x03,0x00,0x00,0x00,0x03,0xFF,0xFF};
	 // enter address to read from
	 for(size_t i = 1;i < 5; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	uint8_t DataOut[25];
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	HAL_SPI_TransmitReceive(&hspi2,Command_Address_Data,DataOut,25,HAL_GetTick()); // Read Status
	uint8_t Check_Start_Packet[20];
	for(size_t j = 5; j < 25; j ++){
		Check_Start_Packet[j-5] = DataOut[j];
	}
	size_t i = 0;
	_Bool check = 1;
	while(check && i < 20){
		check = (Check_Start_Packet[i] == DataOut[i]);
		i++;
	}
	return check;
}

void Set_4Byte_Address(){
	uint8_t Address_Size[1] = {0xB7};
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi2,Address_Size,1,500); // Set Address Size to 4 bytes
	uint8_t ConfReg = Read_Mem_ConfigReg();
	if( (ConfReg >> 5) & 1){
	}
	else{
		Set_4Byte_Address();
	}
}

void Mem_Write_Enable(){
	uint8_t Write_Enable[1] = {0x06};
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
    HAL_SPI_Transmit(&hspi2,Write_Enable,1,500); // Write Enable
}

uint8_t Read_Mem_SecReg(){
	uint8_t RDSCUR[1] = {0x2B};
	uint8_t SecReg[1] = {0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	HAL_SPI_TransmitReceive(&hspi2,RDSCUR,SecReg,1,500); // Write Enable
	return SecReg[0];
}

uint8_t Read_Mem_ConfigReg(){
	uint8_t RDCR[2] = {0x15,0x00};
	uint8_t ConfReg[2] = {0x00,0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_0,GPIO_PIN_RESET); // CS Low
	HAL_SPI_TransmitReceive(&hspi2,RDCR,ConfReg,2,HAL_GetTick()); // Read Status
	return ConfReg[1];
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
